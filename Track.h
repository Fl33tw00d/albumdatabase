/*****************************************************************************

File        : Track.h

Date        : Thursday 28th March 2019

Description : Track class declaration and inline support functions

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#include <string>

#include "Duration.h"
#ifndef TRACK_H
#define TRACK_H

class Track {
    Duration trackDuration;
    std::string trackTitle;

public:
    Track(Duration trackDuration, std::string trackTitle);
    Duration getTrackDuration() const;
    std::string getTrackTitle() const;
    void TrackTest() const;
};

//CONSTRCUTOR
inline Track::Track(Duration d1 = Duration(), std::string trackTitle = "default") {
    this->trackDuration = d1;
    this->trackTitle = trackTitle;
}

//GETTERS
inline Duration Track::getTrackDuration() const {
    return trackDuration;
}

inline std::string Track::getTrackTitle() const {
    return trackTitle;
}

inline std::ostream& operator<<(std::ostream& os, const Track &t) {
    return os << std::setw(7) << t.getTrackDuration() << " - "
            << t.getTrackTitle();
}

std::istream& operator>>(std::istream& is, Track &t);

#endif /* TRACK_H */

