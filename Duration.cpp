/*****************************************************************************

File        : Duration.cpp

Date        : Thursday 28th March 2019

Description : Duration

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#include <iostream>
#include <iomanip>

#include "Duration.h"
using namespace std;

Duration::operator int() const {
    int totalTime = seconds;
    totalTime += minutes * 60;
    totalTime += hours * 3600;
    return totalTime;
}

//OVERLOADING INPUT STREAM
istream& operator>>(istream& is, Duration &d) {
    char c1, c2;

    int hours, minutes, seconds;

    if (is >> hours >> c1 >> minutes >> c2 >> seconds) {
        if (c1 == ':' && c2 == ':') {
            d = Duration(hours, minutes, seconds);
        } else {
            is.clear(ios_base::failbit);
        }
    }

    return is;
}

//TESTING
void Duration::DurationTest() const {
    Duration D1(1, 0, 30);
    Duration D2(1, 20, 39);
    Duration D3 = D1 + D2;

    cout << "Duration One: " << D1 << endl;
    cout << "Duration Two: " << D2 << endl;
    cout << "Sum of Duration One and Two: " << D3 << endl;
}
