/*****************************************************************************

File        : Album.cpp

Date        : Thursday 28th March 2019

Description : Album class

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#include <iostream>
#include <iomanip>
#include <string>

#include "Track.h"
#include "Duration.h"
#include "Album.h"
using namespace std;

//OVERLOADING INPUT STREAM
istream& operator>>(istream& is, Album &A) {
    char delim1 = ':';
    std::string albumArtist, albumTitle;

    if (is) {
        getline(is, albumArtist, delim1);
        if (is) {
            getline(is, albumTitle);
            albumArtist = albumArtist.substr(0, albumArtist.length() - 1);
            albumTitle = albumTitle.substr(1, albumTitle.length());
            A = Album(albumArtist, albumTitle);
        }
    } else {
        is.clear(std::ios_base::failbit);
    }
    return is;
}

void Album::AlbumTest() const {
    vector<Track> tVec;
    Duration D1(1, 0, 30);
    Duration D2(1, 20, 39);
    Track T1(D1, "Test Track One");
    Track T2(D2, "Test Track Two");
    tVec.push_back(T1);
    tVec.push_back(T2);
    Album a1("Test Album", "Test Artist", tVec);
    cout << a1;
    cout << "Total Album Time: " << a1.getAlbumPlayTime() << endl;
}

