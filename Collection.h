/*****************************************************************************

File        : Collection.h

Date        : Thursday 28th March 2019

Description : Collection class declaration and inline support functions

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#ifndef COLLECTION_H
#define COLLECTION_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm> 
#include <vector>

#include "Album.h"
#include "Track.h"
#include "Duration.h"

class Collection {
    std::vector<Album> albumCollection;

public:
    Collection(std::vector<Album> albumCollection);
    std::vector<Album> getAlbumCollection() const;
    void addAlbum(Album a);
    void sortAlphabetical();
    Duration getTotalArtistPlayTime(std::string s) const;
    Album getLargestAlbum() const;
    Track getLongestTrack() const;
};

//CONSTRUCTOR
inline Collection::Collection(std::vector<Album> albumCollection = std::vector<Album>()) {
    this -> albumCollection = albumCollection;
}

//GETTERS
inline std::vector<Album> Collection::getAlbumCollection() const {
    return albumCollection;
}

//OPERATIONS
inline void Collection::addAlbum(Album a) {
    albumCollection.push_back(a);
}

inline void Collection::sortAlphabetical() {
    std::sort(albumCollection.begin(), albumCollection.end());
}

inline std::ostream& operator<<(std::ostream& os, const Collection &C) {
    for (int i = 0; i != C.getAlbumCollection().size(); ++i) {
        os << C.getAlbumCollection().at(i) << std::endl;
    }
    return os;
}

#endif /* COLLECTION_H */

