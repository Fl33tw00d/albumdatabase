/*****************************************************************************

File        : Album.h

Date        : Thursday 28th March 2019

Description : Album class declaration and inline support functions

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#ifndef ALBUM_H
#define ALBUM_H
#include "Track.h"
#include "Duration.h"

#include <iostream>
#include <iomanip>
#include <vector>

class Album {
    std::string albumArtist;
    std::string albumTitle;
    std::vector<Track> albumTrackList;
public:
    Album(std::string albumArtist, std::string albumTitle, std::vector<Track> albumTrackList);
    std::string getAlbumArtist() const;
    std::string getAlbumTitle() const;
    std::vector<Track> getAlbumTrackList() const;
    void addTrack(Track t);
    Duration getAlbumPlayTime() const;
    int getNumberOfTracks()const;
    Track getLongestTrack()const;
    void AlbumTest() const;
};

//CONSTRUCTOR
inline Album::Album(std::string albumArtist = "DEFAULT", std::string albumTitle = "DEFAULT", const std::vector<Track> albumTrackList = std::vector<Track>()) {
    this->albumArtist = albumArtist;
    this->albumTitle = albumTitle;
    this->albumTrackList = albumTrackList;
}
//GETTERS
inline std::string Album::getAlbumArtist() const {
    return albumArtist;
}

inline std::string Album::getAlbumTitle() const {
    return albumTitle;
}

inline std::vector<Track> Album::getAlbumTrackList() const {
    return albumTrackList;
}

inline Duration Album::getAlbumPlayTime() const {
    int totalSeconds = 0;
    for (Track t : albumTrackList) {
        totalSeconds += t.getTrackDuration();
    }
    Duration totalPlayTime(0, 0, totalSeconds);
    return totalPlayTime;
}

//OPERATIONS
inline void Album::addTrack(Track t) {
    albumTrackList.push_back(t);
}

inline int Album::getNumberOfTracks() const {
    return albumTrackList.size();
}

inline Track Album::getLongestTrack() const {
    Track longestTrack = Track(0);
    for (Track t : albumTrackList) {
        if (t.getTrackDuration() > longestTrack.getTrackDuration()) {
            longestTrack = t;
        }
    }
    return longestTrack;
}

//RELATIONAL OPERATORS
inline bool operator==(const Album &A1, const Album &A2) {
    return A1.getAlbumArtist() == A2.getAlbumArtist();
}

inline bool operator!=(const Album &A1, const Album &A2) {
    return !(A1 == A2);
}

inline bool operator>(const Album& A1, const Album &A2) {
    if (A1.getAlbumArtist() > A2.getAlbumArtist()) {
        return true;
    } else {
        if (A1 == A2) {
            if (A1.getAlbumTitle() < A2.getAlbumTitle()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

inline bool operator<(const Album& A1, const Album & A2) {
    if (A1.getAlbumArtist() < A2.getAlbumArtist()) {
        return true;
    } else {
        if (A1 == A2) {
            if (A1.getAlbumTitle() < A2.getAlbumTitle()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

inline std::ostream& operator<<(std::ostream& os, const Album & A) {
    os << A.getAlbumArtist() << " : " << A.getAlbumTitle() << std::endl;
    for (int i = 0; i != A.getAlbumTrackList().size(); ++i) {
        os << A.getAlbumTrackList().at(i) << std::endl;
    }
    return os;
}

std::istream& operator>>(std::istream& is, Album & A);
#endif /* ALBUM_H */

