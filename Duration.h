/*****************************************************************************

File        : Duration.h

Date        : Thursday 28th March 2019

Description : Duration class declaration and inline support functions

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#ifndef DURATION_H
#define DURATION_H

class Duration {
    int hours, minutes, seconds;

public:
    Duration(int hours, int minutes, int seconds);

    int getHours() const;
    int getMinutes() const;
    int getSeconds() const;

    operator int() const;
    void DurationTest() const;
};

//CONSTRCUTOR
inline Duration::Duration(int hours = 0, int minutes = 0, int seconds = 0) {
    //perform my seconds minutes roll over here
    int totalSeconds = (hours * 3600) + (minutes * 60) + seconds;

    this ->hours = totalSeconds / 3600;
    this ->minutes = totalSeconds % 3600 / 60;
    this ->seconds = totalSeconds % 3600 % 60;
}

//GETTERS
inline int Duration::getHours() const {
    return hours;
}

inline int Duration::getMinutes() const {
    return minutes;
}

inline int Duration::getSeconds() const {
    return seconds;
}

//RELATIONAL OPERATORS
inline int operator-(const Duration& d1, const Duration &d2) {
    return static_cast<int> (d1) - static_cast<int> (d2);
}

inline Duration operator+(const Duration& d1, const Duration &d2) {
    int tSeconds = static_cast<int> (d1) + static_cast<int> (d2);
    Duration dTest(0, 0, tSeconds);
    return dTest;
}

inline bool operator==(const Duration& d1, const Duration &d2) {
    return static_cast<int> (d1) == static_cast<int> (d2);
}

inline bool operator!=(const Duration& d1, const Duration &d2) {
    return !(d1 == d2);
}

inline bool operator>(const Duration& d1, const Duration &d2) {
    return static_cast<int> (d1) > static_cast<int> (d2);
}

inline bool operator<(const Duration& d1, const Duration &d2) {
    return static_cast<int> (d1) < static_cast<int> (d2);
}

inline bool operator>=(const Duration& d1, const Duration &d2) {
    return !(d1 < d2);
}

inline bool operator<=(const Duration& d1, const Duration &d2) {
    return !(d1 > d2);
}

inline std::ostream& operator<<(std::ostream& os, const Duration &d) {
    return os << std::setfill('0')
            << std::setw(1) << d.getHours() << ":"
            << std::setw(2) << d.getMinutes() << ":"
            << std::setw(2) << d.getSeconds();
}

std::istream& operator>>(std::istream& is, Duration &d);
#endif /* DURATION_H */

