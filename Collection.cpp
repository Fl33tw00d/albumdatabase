/*****************************************************************************

File        : Collection.cpp

Date        : Thursday 28th March 2019

Description : Collection

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <fstream>

#include "Track.h"
#include "Duration.h"
#include "Album.h"
#include "Collection.h"
using namespace std;

//OPERATIONS
Duration Collection::getTotalArtistPlayTime(string albumArtist = " ") const {
    int collectionPlayTime = 0;

    for (Album a : albumCollection) {
        if (a.getAlbumArtist() == albumArtist) {
            collectionPlayTime += a.getAlbumPlayTime();
        }
    }

    Duration totalPlayTime(0, 0, collectionPlayTime);
    return totalPlayTime;
}

Album Collection::getLargestAlbum() const {
    int currentLargest = 0;
    int index = -1;
    Album inspAlbum = Album();
    //LINEAR COMPARISON
    for (int i = 0; i < albumCollection.size(); i++) {
        inspAlbum = albumCollection.at(i);
        if (inspAlbum.getNumberOfTracks() > currentLargest) {
            currentLargest = inspAlbum.getNumberOfTracks();
            index = i;
        }
    }
    return albumCollection.at(index);
}

Track Collection::getLongestTrack() const {
    Track longestTrack = Track(0);
    for (Album a : albumCollection) {
        if (a.getLongestTrack().getTrackDuration() > longestTrack.getTrackDuration()) {
            longestTrack = a.getLongestTrack();
        }
    }
    return longestTrack;
}

