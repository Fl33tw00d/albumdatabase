/*****************************************************************************

File        : main.cpp

Date        : Thursday 28th March 2019

Description : Main class

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "Duration.h"
#include "Track.h"
#include "Album.h"
#include "Collection.h"
using namespace std;

int main(int argc, char** argv) {
    //DURATION TEST HARNESS
    cout << "DURATION TESTING" << endl;
    Duration TestDuration;
    TestDuration.DurationTest();

    //TRACK TEST HARNESS
    cout << endl << "TRACK TESTING" << endl;
    Track TestTrack;
    TestTrack.TrackTest();

    //ALBUM TEST HARNESS
    cout << endl << "ALBUM TESTING" << endl;
    Album AlbumTest;
    AlbumTest.AlbumTest();

    //COLLECTION TEST HARNESS
    cout << endl << "COLLECTION TESTING" << endl;
    
    //READING IN THE FILE INTO A LINE VECTOR
    Collection C;
    string fileName = "albums.txt";
    ifstream fileInput(fileName);
    vector<string> lineVector;
    string line;
    while (fileInput) {
        getline(fileInput, line);
        lineVector.push_back(line);
    }

    //READING THE LINE VECTOR INTO THE ALBUM OBJECTS USING A STRING 
    //STREAM TO LOAD THE INFORMATION INTO THE DESIRED OBJECTS.
    Album tempAlbum("NULL");
    Track tempTrack(0);
    for (int i = 0; i < lineVector.size() - 1; i++) {
        string line2;
        line2 = lineVector.at(i);
        if (isalpha(line2[0])) {
            if (i != 0) {
                C.addAlbum(tempAlbum);
            }
            istringstream iss(line2);
            iss >> tempAlbum;
        } else {
            istringstream iss(line2);
            iss >> tempTrack;
            tempAlbum.addTrack(tempTrack);
        }
    }
    //ADDING THE FINAL ALBUM
    C.addAlbum(tempAlbum);
    
    //QUESTION TESTING
    C.sortAlphabetical();

    cout << C;
    string pf = "Pink Floyd";
    cout << "Total Play Time for Artist " << pf << " - " << C.getTotalArtistPlayTime(pf) << endl;
    cout << endl;

    Album largestAlbum = C.getLargestAlbum();
    cout << "The Largest Album with a length of " << largestAlbum.getNumberOfTracks() << " tracks: " << endl;
    cout << largestAlbum << endl;
    cout << "The longest track in the Collection: " << C.getLongestTrack();
    return EXIT_SUCCESS;
}

