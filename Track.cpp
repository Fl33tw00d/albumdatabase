/*****************************************************************************

File        : Track.cpp

Date        : Thursday 28th March 2019

Description : 

Author      : Christopher Fleetwood

History     : 28/03/2019 - v1.00
 *            27/04/2019 - v1.10

 ******************************************************************************/
#include <iostream>
#include <iomanip>

#include "Track.h"
using namespace std;

//OVERLOADING INPUT STREAM
istream& operator>>(istream& is, Track &t) {
    char c1;
    Duration trackDuration;
    string trackTitle;
    if (is >> trackDuration >> c1 >> ws) {
        if (c1 == '-') {
            getline(is, trackTitle);
            t = Track(trackDuration, trackTitle);
            return is;
        } else {
            is.clear(std::ios_base::failbit);
        }
    }

}

//TESTING
void Track::TrackTest() const {
    Duration D1(1, 0, 30);
    Duration D2(1, 20, 39);
    Track T1(D1, "Test Track One");
    Track T2(D2, "Test Track Two");
    cout << T1 << endl;
    cout << T2 << endl;
}